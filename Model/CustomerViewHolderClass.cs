﻿using System;
using Android.Views;

namespace AndroidDemoPoc
{
	internal class ViewHolderCategoryClass:Java.Lang.Object
	{
		View view;
		public Action viewClicked{ get; set;}
		public void initialize(View view)
		{
			view.Click += delegate
			{
				viewClicked();
			};
		}

	}
}

