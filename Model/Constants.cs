﻿using System;
using System.Text.RegularExpressions;

namespace AndroidDemoPoc
{
	public static class Constants
	{
		//General
		public const string strAppName="Demo Poc";
		public const string strNoInternet="No internet connection. Make sure your network connectivity in active and try again.";
		public const string strMandatoryFields="You must fill in all of the fields.";
		public const string strLoginFaild="Login Faild";
		public const string strEmailIdError="Invalid mail id";
		public const string strPasswordNotMatch="Password does not match the confirm password";

		public const string strUpdated="Customer details updated";
		public const string strNotUpdated="Unable to update customer details at this moment!";
		public const string strDeleted="Customer details deleted";
		public const string strNotDeleted="Unable to delete customer details at this moment!";

		public const string strYouSureDelete="Are you sure want to delete customer record?";
		public const string strYes="Yes";
		public const string strNo="No";

		public const string strRegistered="Registered successfully";
		public const string strNoRegistered="Unable to Register at this moment!";

		public const string strSaved="Saved successfully";
		public const string strNoSave="Unable to Save at this moment!";
	}
}