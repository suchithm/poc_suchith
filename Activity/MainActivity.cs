using System;
using Android.App;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;
using RestSharp;
using System.Collections.Generic;
using POC.EntityModel;
using Android.Widget;
using Android.Graphics.Drawables;

namespace AndroidDemoPoc
{
	[Activity ( Icon = "@mipmap/icon")]
	public class MainActivity : AppCompatActivity
	{
		DrawerLayout drawerLayout;
		RestClient _client;
		//ListView listCustomer;
		NavigationView navigationView;
		string WEBAPI_URL  ="http://202.83.22.139/POCWebApi"; 
		protected override void OnCreate(Bundle bundle)
		{			
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.Main);

			FnInitilization ();
			//FnClickEvent ();

		}
		public override bool OnCreateOptionsMenu (Android.Views.IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu,menu);
			if (menu != null) {
				menu.FindItem (Resource.Id.action_email).SetVisible (true);
				menu.FindItem (Resource.Id.action_flag).SetVisible (true); 
				menu.FindItem (Resource.Id.action_profile).SetVisible (true);
			}
			return base.OnCreateOptionsMenu (menu);
		}

		public override void OnBackPressed ()
		{
			if(FragmentManager.BackStackEntryCount!= 0) {
				FragmentManager.PopBackStack ();// fragmentManager.popBackStack();
			} else {
				base.OnBackPressed ();
			}  
		}

		void FnInitilization()
		{
			drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			//listCustomer = FindViewById<ListView> (Resource.Id.listView1);
			navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
			navigationView.NavigationItemSelected += FnNavigationItemSelected;
			// Init toolbar
			var toolbar = FindViewById<Toolbar>(Resource.Id.app_bar);
			SetSupportActionBar(toolbar);

			ColorDrawable colorDrawable = new ColorDrawable(Android.Graphics.Color.Rgb(54,127,169)); 
			SupportActionBar.SetBackgroundDrawable (colorDrawable);
			// Create ActionBarDrawerToggle button and add it to the toolbar
			var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, Resource.String.open_drawer, Resource.String.close_drawer);
			drawerLayout.SetDrawerListener(drawerToggle);
			drawerToggle.SyncState();

			var ft= FragmentManager.BeginTransaction ();
			ft.AddToBackStack (null);
			ft.Add (Resource.Id.HomeFrameLayout, new DashBoardFragment ());
			ft.Commit ();

			_client = new RestClient (WEBAPI_URL);
		}
		void FnClickEvent()
		{
			navigationView.NavigationItemSelected += FnNavigationItemSelected;
		}

		void FnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
		{
			switch (e.MenuItem.ItemId)
			{
			case (Resource.Id.nav_home):
				var ft= FragmentManager.BeginTransaction ();
				ft.AddToBackStack (null);
				ft.Replace (Resource.Id.HomeFrameLayout, new DashBoardFragment ());
				ft.Commit ();
					break;
			case (Resource.Id.nav_messages):
				var ft2= FragmentManager.BeginTransaction ();
				ft2.AddToBackStack (null);
				ft2.Replace (Resource.Id.HomeFrameLayout, new CustomerFragment ());
				ft2.Commit ();
					break;   
			case (Resource.Id.nav_Logout):
				StartActivity (typeof(LoginActivity));
				Finish ();
				break;                
			}

			// Close drawer
			drawerLayout.CloseDrawers();
		}

//		public void FnGetCustomerList()
//		{
//			var request = new RestRequest("api/Customer", Method.GET) { RequestFormat = DataFormat.Json };
//			request.AddHeader("Token",LoginActivity.strToken);
//
//			var response = _client.Execute<List<CustomerModel>>(request);
//
//			if (response.Data != null) {
//				//response.Data is list of data of type CustomerModel
//				CustomerAdaptorClass objCustomerAdaptorClass=new CustomerAdaptorClass (this,response.Data);
//				listCustomer.Adapter = objCustomerAdaptorClass;
//				objCustomerAdaptorClass.CustomerNameClicked+=CustomerRowTapped;
//			} 
//
//		}
		void CustomerRowTapped(CustomerModel objCustomerModel)
		{
			
		}
	}
}