﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using RestSharp;
using System.Net;
using POC.EntityModel;

namespace AndroidDemoPoc
{
	[Activity ( Label = "Demo Poc",MainLauncher = true )]			
	public class LoginActivity : Activity
	{
		Button btnSignIn;
		EditText txtEmail;
		EditText txtPassword;
		TextView lblForgotPassword;
		TextView lblRegisterNewMemberShip;
		CheckBox chkRememberMe;

		ProgressDialog progress;
		internal static string strToken=string.Empty;
		RestClient _client;
		string WEBAPI_URL  ="http://202.83.22.139/POCWebApi"; 
		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.LoginLayout );
			FnInitilization ();
			FnClickEvent ();
		}
		void FnInitilization()
		{
			btnSignIn = FindViewById<Button> ( Resource.Id.btnSignInLL );
			txtEmail=FindViewById<EditText> ( Resource.Id.txtEmailLL);
			txtPassword=FindViewById<EditText> ( Resource.Id.txtPasswordLL );
			chkRememberMe = FindViewById<CheckBox> (Resource.Id.chkRemberMeLL);
			lblForgotPassword = FindViewById<TextView> ( Resource.Id.lblForgotPAsswordLL );
			lblRegisterNewMemberShip = FindViewById<TextView> ( Resource.Id.lblRegisterNewMemberShipLL );

			_client = new RestClient (WEBAPI_URL);

			txtEmail.Text="anush3@itwinetech.com";
			txtPassword.Text="anush3";
		}

		void FnClickEvent()
		{
			btnSignIn.Click += async delegate(object sender , EventArgs e )
			{
				bool isMandatoryFields = CommonClass.FnFieldsValidation ( txtEmail.Text , txtPassword.Text );
				if ( isMandatoryFields )
				{
					CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMandatoryFields , this );
				}
				else
				{
					bool isMailValidation = CommonClass.FnEmailValidation ( txtEmail.Text );
					if ( !isMailValidation )
					{
						CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strEmailIdError , this );
					}
					else
					{
						bool isConnected = CommonClass.FnIsConnected ( this );
						if ( !isConnected )
						{
							CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strNoInternet , this );
						}
						else
						{
							bool isSuccess = await SignIn ( txtEmail.Text , txtPassword.Text );
							if ( isSuccess )
							{
								
							}
							else
							{
								CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strLoginFaild , this );
							}
						}
					}
				}
			};
			lblForgotPassword.Click+=delegate(object sender , EventArgs e )
			{
				
			};
			lblRegisterNewMemberShip.Click+=delegate(object sender , EventArgs e )
			{
				StartActivity(typeof (SignUpActivity));
			};
		}

		async Task<bool> SignIn(string strEmailId,string strPassword)
		{  
			try
			{
				RunOnUiThread ( () =>
				{
					progress = ProgressDialog.Show ( this , "" , "Please wait..." );
				} );
				var request = new RestRequest ( "api/login" , Method.POST ) { RequestFormat = DataFormat.Json };
				request.AddParameter ( "Authorization" , string.Concat ( "Basic " , POC.Utilities.CommonFunctions.EncryptData ( strEmailId + ":" + strPassword ) ) , ParameterType.HttpHeader );
				string strdataP = POC.Utilities.CommonFunctions.EncryptData ( strEmailId + ":" + strPassword );
				//_client.Authenticator = new NtlmAuthenticator ();
				_client.ExecuteAsync ( request , response =>
				{
					if ( !string.IsNullOrEmpty ( response.Content ) )
					{
						Console.WriteLine ( response.Content );
						strToken = response.Headers.FirstOrDefault ( a => a.Name == "Token" ).Value.ToString ();
							if(!string.IsNullOrEmpty( strToken))
							StartActivity ( typeof ( MainActivity ) );
					}
					RunOnUiThread ( () =>
					{
						if ( progress != null )
						{
							progress.Dismiss ();
							progress = null;
						}
					} );
				});
			}
			catch
			{
				RunOnUiThread ( () =>
				{
					if ( progress != null )
					{
						progress.Dismiss ();
						progress = null;
					}
				} );
				return false;
			}
			return true;
		}


	}
}

