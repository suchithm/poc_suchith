﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidDemoPoc
{
	[Activity (Label = "DemoPOC",MainLauncher = true )]			
	public class LandingActivity : Activity
	{
		Button btnSign;
		Button btnRegister;
		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.LandingLayout );
			// Create your application here
			btnSign = FindViewById<Button> ( Resource.Id.btnSignFL );
			btnRegister = FindViewById<Button> ( Resource.Id.btnRegisterFL );


			btnSign.Click += delegate(object sender , EventArgs e )
			{
				StartActivity(typeof(LoginActivity));
			};
			btnRegister.Click += delegate(object sender , EventArgs e )
			{
				StartActivity(typeof(SignUpActivity));
			};
		}

	}
}

