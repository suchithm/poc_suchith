﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using POC.EntityModel;
using RestSharp;
using System.Net;

namespace AndroidDemoPoc
{
	[Activity ( Label = "SignUp" )]			
	public class SignUpActivity : Activity
	{
		EditText txtFirstName;
		EditText txtLastName;
		EditText txtEmailId;
		EditText txtPassword;
		EditText txtRetypePassword;
		EditText txtDob;
		EditText txAddress;
		Button btnRegister;
		CheckBox chkTermsCondition;

		bool isChecked=false;
		RestClient _client;
		UserRegistrationModel objUserRegistrationModel;
		string WEBAPI_URL  ="http://202.83.22.139/POCWebApi"; 

		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.SignUpLayout );
			FnInitilization ();
			FnClickEvent ();
		}

		void FnInitilization()
		{
			btnRegister = FindViewById<Button> (Resource.Id.btnRegisterSUL);
			txtFirstName = FindViewById<EditText> (Resource.Id.txtFirstNameSUL);
			txtLastName = FindViewById<EditText> (Resource.Id.txtLastNameSUL);
			txtEmailId = FindViewById<EditText> (Resource.Id.txtEmailSUL);
			txtPassword = FindViewById<EditText> (Resource.Id.txtPasswordSUL);
			txtRetypePassword = FindViewById<EditText> (Resource.Id.txtRetypePasswordSUL);
			chkTermsCondition = FindViewById<CheckBox> (Resource.Id.checkBoxSUL);
			txtDob = FindViewById<EditText> (Resource.Id.txtDOBSignUp);
			txAddress = FindViewById<EditText> (Resource.Id.txAddressSUL);
			_client = new RestClient (WEBAPI_URL);
			objUserRegistrationModel = objUserRegistrationModel ?? new UserRegistrationModel (); 
		}

		void FnClickEvent()
		{
			btnRegister.Click += delegate(object sender , EventArgs e )
			{
				bool isMandatoryFields = CommonClass.FnSignUpFieldsValidation ( txtFirstName.Text , txtLastName.Text , txtEmailId.Text , txtPassword.Text , txtRetypePassword.Text );
				if ( isMandatoryFields )
				{
					CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMandatoryFields , this );
				}
				else
				{
					bool isMailValidation = CommonClass.FnEmailValidation ( txtEmailId.Text );
					if ( !isMailValidation )
					{
						CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strEmailIdError , this );
					}
					else
					{
						if ( !(txtPassword.Text == txtRetypePassword.Text ))
						{
							CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strPasswordNotMatch , this );
						}
						else
						{
							bool isConnected = CommonClass.FnIsConnected ( this );
							if ( !isConnected )
							{
								CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strNoInternet , this );
							}
							else
							{
								objUserRegistrationModel.FirstName = txtFirstName.Text;
								objUserRegistrationModel.LastName = txtLastName.Text;
								objUserRegistrationModel.EmailId = txtEmailId.Text;
								objUserRegistrationModel.Password = txtPassword.Text;
								objUserRegistrationModel.ConfirmPassword = txtRetypePassword.Text; 
								objUserRegistrationModel.IAgreeToTerms = isChecked;  
								FnRegistration ( objUserRegistrationModel );
							}
						}
					}
				}
			};
			chkTermsCondition.CheckedChange+= delegate(object sender , CompoundButton.CheckedChangeEventArgs e )
			{
				isChecked=e.IsChecked;
			};
			txtDob.Click += delegate(object sender , EventArgs e )
			{
				ShowDialog(0);
			};

		}
		void FnRegistration(UserRegistrationModel customer)
		{ 
			try {
				string strAlertMsg = string.Empty;
				var request = new RestRequest ("api/customer/Register", Method.POST) { RequestFormat = DataFormat.Json };
				request.AddJsonBody (customer);
				var response = _client.Execute (request);
				if (response.StatusCode == HttpStatusCode.Created) {
					strAlertMsg = Constants.strRegistered; 
					StartActivity(typeof(LoginActivity));
				} else { 
					strAlertMsg = Constants.strNoRegistered; 
				}

				CommonClass.FnAlertMssg (Constants.strAppName, strAlertMsg, this);
			} catch {
				CommonClass.FnAlertMssg (Constants.strAppName, Constants.strNoRegistered, this);
			}
		}

		protected override Dialog OnCreateDialog (int id)
		{
			return new DatePickerDialog (this,handleDate,DateTime.Today.Year,DateTime.Today.Month,DateTime.Today.Day);
		}
		void handleDate(object sender,DatePickerDialog.DateSetEventArgs e)
		{
			txtDob.Text = e.Date.ToString("u").Split(' ',(char) StringSplitOptions.RemoveEmptyEntries)[0];
		}  
	}
}

