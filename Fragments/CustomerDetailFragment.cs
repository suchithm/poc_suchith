﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using POC.EntityModel;
using RestSharp;
using System.Net;

namespace AndroidDemoPoc
{
	public class CustomerDetailFragment : Fragment
	{
		RestClient _client;
		string WEBAPI_URL  ="http://202.83.22.139/POCWebApi"; 
		//Update Initilization
		EditText txtFirstName;
		EditText txtLastName;
		EditText txtEmailId;
		EditText txtMobileNo;
		Button btnUpdate;
		Button btnDelete;
		EditText txtDOB;
		EditText txtAddress;
		DateTime date;
		const int Date_Id = 0;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);
			View view=inflater.Inflate(Resource.Layout.CustomerDetailLayout,container,false);

			//Update Initilization 
			txtFirstName = view.FindViewById<EditText> (Resource.Id.txtFirstNameUCL);
			txtLastName = view.FindViewById<EditText> (Resource.Id.txtLastNameUCL);
			txtEmailId = view.FindViewById<EditText> (Resource.Id.txtEmailUCL);
			txtMobileNo = view.FindViewById<EditText> (Resource.Id.txtMobileNoUCL);
			btnUpdate=view.FindViewById<Button> (Resource.Id.btnUpdateUCL);
			btnDelete=view.FindViewById<Button> (Resource.Id.btnDeleteUCL);
			txtDOB = view.FindViewById<EditText> (Resource.Id.txtDobUCL);
			txtAddress = view.FindViewById<EditText> (Resource.Id.txtAddressUCL);

			_client = new RestClient( WEBAPI_URL);

			if (CustomerFragment.objCustomerModel != null) {
				if (!CustomerFragment.isSave) {
					FnFillCustomerData (CustomerFragment.objCustomerModel);
				}
			}
			btnUpdate.Click+= delegate(object sender, EventArgs e) {
				if (CustomerFragment.objCustomerModel != null)
				if(CustomerFragment.isSave)
				{
					//for save
					FnSave(CustomerFragment.objCustomerModel);
				}else
				{
					//update
				
					FnUpdate(CustomerFragment.objCustomerModel );
				}
				
			};
			btnDelete.Click+= delegate(object sender, EventArgs e) {
				if (CustomerFragment.objCustomerModel != null)
				fnAlertMsgTwoInput(Constants.strAppName,Constants.strYouSureDelete,Constants.strYes,Constants.strNo,this.Activity);
			};
			txtDOB.Click += delegate
			{       
				//ShowDialog(Date_Id);
				//alternative method
				CreateDialog (Date_Id).Show ();
			};
			return view;
		}
		 public override void OnResume ()
		{ 
			((AppCompatActivity)this.Activity).SupportActionBar.SetTitle (Resource.String.CustomersDtl);
			base.OnResume ();
		}
		protected Dialog CreateDialog (int id)
		{
			switch ( id )
			{
			case Date_Id:
				return new DatePickerDialog (this.Activity, OnDateSet, date.Year, date.Month-1, date.Day); 
			}
			return null;
		}
		void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
		{
			this.date = e.Date;
			UpdateDisplay ();
		}
		  void UpdateDisplay ()
		{
			txtDOB.Text = date.ToString ();
		}
		void FnFillCustomerData(CustomerModel objCustomerModel)
		{ 
			txtFirstName.Text = objCustomerModel.FirstName;
			txtLastName.Text = objCustomerModel.LastName;
			txtEmailId.Text = objCustomerModel.EmailId;
			txtMobileNo.Text = objCustomerModel.Mobile; 
			txtDOB.Text = objCustomerModel.DOB.ToString();
			txtAddress.Text=objCustomerModel.Address;
		}

		public void FnUpdate(CustomerModel customer)
		{
			try {
				customer.FirstName=txtFirstName.Text;// = objCustomerModel.FirstName;
				customer.LastName=txtLastName.Text;// = objCustomerModel.LastName;
				customer.EmailId=txtEmailId.Text;// = objCustomerModel.EmailId;
				customer.Mobile=txtMobileNo.Text;// = objCustomerModel.Mobile; 
				customer.DOB=Convert.ToDateTime( txtDOB.Text);// = objCustomerModel.DOB.ToString();
				customer.Address=txtAddress.Text;//=objCustomerModel.Address;
				 

				string strAlertMsg = string.Empty;

				var request = new RestRequest ("api/Customer/Update", Method.POST) { RequestFormat = DataFormat.Json };
				request.AddHeader ("Token", LoginActivity.strToken);
				request.AddJsonBody (customer); 
		
				var response = _client.Execute (request); 
		

				if (response.StatusCode == HttpStatusCode.OK) {
					
					strAlertMsg = Constants.strUpdated;
					FnLoadCustomerFragment();
				} else {
					strAlertMsg = Constants.strNotUpdated;
				}

				CommonClass.FnAlertMssg (Constants.strAppName, strAlertMsg, this.Activity);

		 
			} catch {
				CommonClass.FnAlertMssg (Constants.strAppName, Constants.strNotUpdated, this.Activity);
				 
			}
		}

		internal void FnSave(CustomerModel customer)
		{
			try {
				customer.FirstName =	txtFirstName.Text;
				customer.LastName = txtLastName.Text;
				customer.EmailId =	txtEmailId.Text;
				customer.Mobile = txtMobileNo.Text; 
				customer.DOB = Convert.ToDateTime( txtDOB.Text);
				customer.Address = txtAddress.Text;

				string strAlertMsg = string.Empty;
				var request = new RestRequest ("api/Customer/Create", Method.POST) { RequestFormat = DataFormat.Json };
				request.AddHeader ("Token", LoginActivity.strToken);
				request.AddJsonBody (customer);
				var response = _client.Execute<CustomerModel> (request);

				if (response.StatusCode == HttpStatusCode.Created) { 
					strAlertMsg = Constants.strSaved;
					FnLoadCustomerFragment ();
				
				} else {
					strAlertMsg = Constants.strNoSave;
				
				}
				CommonClass.FnAlertMssg (Constants.strAppName, strAlertMsg, this.Activity);
			} catch {
				CommonClass.FnAlertMssg (Constants.strAppName, Constants.strNoSave, this.Activity);
			}
		}

		internal  void fnAlertMsgTwoInput (string strTitle,string strMsg,string strOk,string strCancel,Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder (context).Create ();
			alertMsg.SetCancelable (false);
			alertMsg.SetTitle (strTitle);
			alertMsg.SetMessage (strMsg);
			alertMsg.SetButton2(strOk, delegate (object sender, DialogClickEventArgs e) 
				{
					if (e.Which  == -2) 
					{
						FnDelete(CustomerFragment.objCustomerModel.CustomerId,CustomerFragment.objCustomerModel);
					}
				});
			alertMsg.SetButton(strCancel,delegate (object sender, DialogClickEventArgs e) 
				{
					if (e.Which  == -1) 
					{
						alertMsg.Dismiss ();
						alertMsg=null;
					}
				});
			alertMsg.Show ();
		} 


		public bool FnDelete(long id, CustomerModel customer)
		{
			try {
				string strAlertMsg = string.Empty;
				var request = new RestRequest ("api/Customer/Delete/{id}", Method.POST);
				request.AddHeader ("Token", LoginActivity.strToken);
				request.AddParameter ("id", id, ParameterType.UrlSegment);
		
				var response = _client.Execute (request);
		
				if (response.StatusCode == HttpStatusCode.OK) {
					strAlertMsg = Constants.strDeleted;
					FnLoadCustomerFragment();

				} else {
					strAlertMsg = Constants.strNotDeleted; 
				}
 
				CommonClass.FnAlertMssg (Constants.strAppName, strAlertMsg, this.Activity);
		 
			} catch {
				CommonClass.FnAlertMssg (Constants.strAppName, Constants.strNotDeleted, this.Activity);
				return false; 	 
			}
			return true;
		}
		void FnLoadCustomerFragment()
		{
			var ft2= FragmentManager.BeginTransaction ();
			ft2.AddToBackStack (null);
			ft2.Replace (Resource.Id.HomeFrameLayout, new CustomerFragment ());
			ft2.Commit ();
		}



	}
}

