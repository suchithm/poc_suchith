﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RestSharp;
using POC.EntityModel;
using Android.Support.V7.App;

namespace AndroidDemoPoc
{
	public class CustomerFragment : Fragment
	{
		RestClient _client;
		string WEBAPI_URL  ="http://202.83.22.139/POCWebApi"; 
		ListView lstCustomers;
		internal static CustomerModel objCustomerModel=null;
		ImageView imgAdd;
		internal static bool isSave=false;
		public override void OnCreate (Bundle savedInstanceState)
		{
			_client = new RestClient( WEBAPI_URL);

			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);
			View view= inflater.Inflate(Resource.Layout.CustomerLayout,container,false);
			lstCustomers = view.FindViewById<ListView> (Resource.Id.lstCustomer);
			imgAdd=view.FindViewById<ImageView> (Resource.Id.imgAdd);
			 
			FnGetCustomerList ();
			imgAdd.Click+= delegate(object sender, EventArgs e) {
				isSave=true;
				FnLoadCustomerDetail();
			};
			return view;
		}
		public override void OnResume ()
		{ 
			((AppCompatActivity)this.Activity).SupportActionBar.SetTitle (Resource.String.Customers);
			base.OnResume ();
		}

		public void FnGetCustomerList()
		{
			var request = new RestRequest("api/Customer", Method.GET) { RequestFormat = DataFormat.Json };
			request.AddHeader("Token",LoginActivity.strToken);

			var response = _client.Execute<List<CustomerModel>>(request);

			if (response.Data != null) 
			{
				//response.Data is list of data of type CustomerModel
				CustomerAdaptorClass objCustomerAdaptorClass=new CustomerAdaptorClass (this.Activity,response.Data);
				lstCustomers.Adapter = objCustomerAdaptorClass;
				objCustomerAdaptorClass.CustomerNameClicked+=FnCustomerRowTapped;

			} 
		}
		void FnCustomerRowTapped(CustomerModel objCustomer)
		{ 
			isSave = false;
			if (objCustomer != null) {
				objCustomerModel = objCustomer;

				FnLoadCustomerDetail ();

			}
		}
		void FnLoadCustomerDetail()
		{
			var ft= FragmentManager.BeginTransaction ();
			ft.AddToBackStack (null);
			ft.Replace (Resource.Id.HomeFrameLayout, new CustomerDetailFragment ());
			ft.Commit ();
		}


	}
}

